# Layered identification Approach

The codebase required to run the identification approach and measure its results is available here. 

In section "C2C Coverage" we cover how to run the c2c coverage measure to measure the quality of Experiment 2 and 3.

In section "MoJoFMMeasure" we cover how to run the MoJoFM measure to measure the quality of Experiment 2 and 3.

In the next section, we describe the steps towards installing and using the layered approach on a set of monolithic applications.

Furthermore, the dataset are available in the project under the folder : `src/main/resources`. Inside the `resources` folder, the ground truth architectures, experiment 2's recovered architectures, and experiment 3's architectures are available. Finally, the architecture recovered by Semalji et al. is available at the path `selmadji`.

## Layered Identification Codecase using Pharo

To run the proposed approach, you must first download Pharo [here](https://pharo.org/download).

![](images/pharo-website.PNG)

Once Pharo has been downloaded and installed, you must unzip `ICSA2022Paper22.zip`. Afterward, you can launch pharo and you should be greeted with the following interface:

![](images/pharo-interface.PNG)

Click the `From Disk button` and select the `ICSA2022Paper22` folder created during the unzipping.

![](images/import.PNG)

If the import is successful, the image should be viewable from the the main screen:

![](images/loaded.PNG)

From there, the image can be selected and launched. If successful, the following interface should appear: 

![](images/interface.PNG)

If the playground appears then the installation has been successful. Next we can manipulate the code to generate the decompositions.

The code can be executed with the following commands:

```smalltalk
"JPetStore identification"
jPetStoreMigration := JPetStoreMigration new 
	initialize;
	import;
	yourself.
jPetStoreMigration integrateNTierArchitecture.
jPetStoreMigration visualizeNTier.
jPetStoreMigration partitionMonolith.
jPetStoreMigration generatePartitionXML: 'withArchitecture'.
```
The first instruction corresponds to the importation of the source code to be analyzed by the program:

```smalltalk
"JPetStore identification"
jPetStoreMigration := JPetStoreMigration new 
	initialize;
	import;
	yourself.
```

The next step is to integrate the layered architecture into the model with the following command:
```smalltalk
jPetStoreMigration integrateNTierArchitecture.
```

For the purpose of RQ1, it is also possible to skip the integration step with the following command:
```smalltalk
jPetStoreMigration skipIntegrateArchitecture.
```
It is possible to have a higher-level vizualization of the layered architecture with the following command, by selecting the line and `CRTL+I` or right clicking and choosing inspect:
```smalltalk
jPetStoreMigration visualizeNTier.
```
Finally, we can partition the monolith with the following command:
```smalltalk
jPetStoreMigration partitionMonolith.
```

Additionally, you may also generate the XML of the partitions with the following instruction (in this case the parameter 'withArchitecture is the folder where the XML will be saved):
```smalltalk
jPetStoreMigration generatePartitionXML: 'withArchitecture'.
```


## C2C Coverage

To measure the C2C coverage of a partition, the pharo image must be running on your computer.

```smalltalk
c2cSimilarity := C2CSimilarity new initialize: 'identifications/withArchitecture/JPetStore.xml' withGroundTruth: 'identifications/ground-truth/JPetStore.xml'; yourself.
c2cSimilarity calculateCoverage.
```
To measure the coverage, the class must be initialized with the path to both architectures. The method `calculateCoverage` returns the values of the coverage based on several different threshold levels.


## MoJoFMMeasure

This java project contains the code necessary to measure the MoJoFM metric on a set of recovered architectures. If you would like to consult the recovered architectures they are available under `src/main/resources`. Inside the `resources` folder, the ground truth architectures, experiment 2's recovered architectures, and experiment 3's architectures are available.

The code used to calculate the measure is taken from [here](http://www.cs.yorku.ca/~bil/downloads/) proposed in Lutellier et Al.'s paper.
To run the measure, you have two options.

### Run the MoJoFMMeasure application using Eclipse

With this method, you can inspect the code, and run it. Just import it into Eclipse and run it through the main class `measure.MeasureRecoveredArchitecture`.

### Run it with docker

To run the application with docker, you must first clone the project.

Then, from your command line type the following line to build the docker image :

`docker build -t mojofm-measure .`

Finally, you can run it from the command line:

`docker run mojofm-measure`

### Generating the RSF files for MoJoFM

If you wish to generate the rsf files required for running the MoJoFM files, the pharo image contains a method to transform the existing XML files into a RSF file:

```smalltalk
mojo := MoJoFMSimilarity new.
mojo generatePartitionRSF: 'identifications/ground-truth' filename: 'jpetstore'.
```

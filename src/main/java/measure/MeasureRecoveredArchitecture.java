package measure;

import mojofm.MoJo;

public class MeasureRecoveredArchitecture {

	public static void main(String[] args) {
		// take two xml files and transform to rsf
		// format of a relation should be "contain clusterID ObjectID"
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
//		String[] paths = {"src/main/resources/max-distance-mojofm/findsportmates.rsf","src/main/resources/ground-truth/findsportmates.rsf", "-fm"};
//		MoJo.calculateMoJo(paths);
		System.out.println("-------------------FindSportMates-----------------");
		String[] paths2 = {"src/main/resources/withArchitecture/findsportmates.rsf","src/main/resources/ground-truth/findsportmates.rsf", "-fm"};
		String[] pathsWithoutArchi = {"src/main/resources/withoutArchitecture/findsportmates.rsf","src/main/resources/ground-truth/findsportmates.rsf", "-fm"};
		System.out.println("Experiment 2 (without layered architecture artifacts) : " + MoJo.calculateMoJo(pathsWithoutArchi));
		System.out.println("Experiment 3 (with layered architecture artifacts) : " + MoJo.calculateMoJo(paths2));
		
		System.out.println("-----------------Production SSM-----------------");
//		String[] paths4 = {"src/main/resources/max-distance-mojofm/production-ssm.rsf","src/main/resources/ground-truth/production-ssm.rsf", "-fm"};
//		MoJo.calculateMoJo(paths4);
		String[] paths5 = {"src/main/resources/withArchitecture/production-ssm.rsf","src/main/resources/ground-truth/production-ssm.rsf", "-fm"};
//		MoJo.calculateMoJo(paths5);
		String[] paths6 = {"src/main/resources/withoutArchitecture/production-ssm.rsf","src/main/resources/ground-truth/production-ssm.rsf", "-fm"};
//		MoJo.calculateMoJo(paths6);
		System.out.println("Experiment 2 (without layered architecture artifacts) : " + MoJo.calculateMoJo(paths6));
		System.out.println("Experiment 3 (with layered architecture artifacts) : " + MoJo.calculateMoJo(paths5));
		
		System.out.println("-----------------SpringBlog-----------------");
//		String[] paths4 = {"src/main/resources/max-distance-mojofm/production-ssm.rsf","src/main/resources/ground-truth/production-ssm.rsf", "-fm"};
//		MoJo.calculateMoJo(paths4);
		String[] paths7 = {"src/main/resources/withArchitecture/springblog.rsf","src/main/resources/ground-truth/springblog.rsf", "-fm"};
//		MoJo.calculateMoJo(paths7);
		String[] paths8 = {"src/main/resources/withoutArchitecture/springblog.rsf","src/main/resources/ground-truth/springblog.rsf", "-fm"};
//		MoJo.calculateMoJo(paths8);
		System.out.println("Experiment 2 (without layered architecture artifacts) : " + MoJo.calculateMoJo(paths8));
		System.out.println("Experiment 3 (with layered architecture artifacts) : " + MoJo.calculateMoJo(paths7));
		
		System.out.println("-----------------JPetStore-----------------");
//		String[] paths4 = {"src/main/resources/max-distance-mojofm/production-ssm.rsf","src/main/resources/ground-truth/production-ssm.rsf", "-fm"};
//		MoJo.calculateMoJo(paths4);
		String[] paths9 = {"src/main/resources/withArchitecture/jpetstore.rsf","src/main/resources/ground-truth/jpetstore.rsf", "-fm"};
//		MoJo.calculateMoJo(paths9);
		String[] paths10 = {"src/main/resources/withoutArchitecture/jpetstore.rsf","src/main/resources/ground-truth/jpetstore.rsf", "-fm"};
//		MoJo.calculateMoJo(paths10);
		System.out.println("Experiment 2 (without layered architecture artifacts) : " + MoJo.calculateMoJo(paths10));
		System.out.println("Experiment 3 (with layered architecture artifacts) : " + MoJo.calculateMoJo(paths9));
	}
}
